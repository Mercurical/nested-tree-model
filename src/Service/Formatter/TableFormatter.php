<?php

namespace App\Service\Formatter;

use App\Model\TableMeta;

class TableFormatter
{
    /**
     * @param array $data
     * @param TableMeta[] $meta
     *
     * @return string
     */
    public function format(array $data, array $meta): string
    {
        $output = $this->header($meta);

        foreach ($data as $row) {
            $output .= $this->formatRow($row, $meta);
        }

        $output .= $this->footer($meta);

        return $output;
    }

    /**
     * @param TableMeta[] $meta
     *
     * @return string
     */
    private function header(array $meta): string
    {
        $header = $this->separator($meta);

        foreach ($meta as $item) {
            $header .= '|' . str_pad(' ' . $item->getColumnName(), $item->getColumnWidth());
        }

        $header .= "|\n";
        $header .= $this->separator($meta);

        return $header;
    }

    /**
     * @param TableMeta[] $meta
     *
     * @return string
     */
    private function footer(array $meta): string
    {
       return $this->separator($meta);
    }

    /**
     * @param array $meta
     *
     * @return string
     */
    private function separator(array $meta): string
    {
        $footer = '';

        foreach ($meta as $item) {
            $footer .= '+' . str_repeat('-', $item->getColumnWidth());
        }

        return $footer . '+' . "\n";
    }

    /**
     * @param array $row
     * @param TableMeta[] $meta
     *
     * @return string
     */
    private function formatRow(array $row, array $meta): string
    {
        $text = '';

        foreach ($meta as $item) {
            $text .= '|' . str_pad(' ' . $row[$item->getColumnKey()] . ' ', $item->getColumnWidth(), ' ', $item->getTextOrientation());
        }

        return $text . '|' . "\n";
    }
}