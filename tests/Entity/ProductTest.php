<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $product = new Product();

        $product->setName('Laczek');
        $this->assertEquals('Laczek', $product->getName());

        $category = new Category();
        $product->setCategory($category);
        $this->assertEquals($product->getCategory(), $category);
    }
}