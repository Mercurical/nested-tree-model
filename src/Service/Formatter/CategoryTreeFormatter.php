<?php

namespace App\Service\Formatter;

use App\Model\CategoryTree;
use App\Model\CategoryTreeNode;

class CategoryTreeFormatter
{
    private const INDENT = '  ';

    /**
     * @param CategoryTree $tree
     *
     * @return string
     */
    public function format(CategoryTree $tree): string
    {
        $output = '';

        foreach ($tree->getChilds() as $child) {
            $output = $this->formatNode($child, $output);
        }

        return $output;
    }

    /**
     * @param CategoryTreeNode $node
     * @param string $output
     *
     * @return string
     */
    public function formatNode(CategoryTreeNode $node, string $output): string
    {
        $output .= str_repeat(self::INDENT, $node->getLevel()) . $node->getName() . "\n";

        foreach ($node->getChilds() as $child) {
            $output = $this->formatNode($child, $output);
        }

        return $output;
    }
}