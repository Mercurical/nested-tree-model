<?php

namespace App\Model;

class TableMeta
{
    public const TEXT_LEFT = 0;
    public const TEXT_RIGHT = 1;

    /**
     * @var string
     */
    private $columnName;

    /**
     * @var string
     */
    private $columnKey;
    /**
     * @var int
     */
    private $columnWidth;

    /**
     * @var int
     */
    private $textOrientation = self::TEXT_RIGHT;

    /**
     * TableMeta constructor.
     *
     * @param string $name
     * @param string $key
     * @param int $width
     * @param int $textOrientation
     */
    public function __construct(string $name, string $key, int $width, int $textOrientation = self::TEXT_RIGHT)
    {
        $this->columnName = $name;
        $this->columnKey = $key;
        $this->columnWidth = $width;
        $this->textOrientation = $textOrientation;
    }

    /**
     * @return string
     */
    public function getColumnName(): string
    {
        return $this->columnName;
    }

    /**
     * @return string
     */
    public function getColumnKey(): string
    {
        return $this->columnKey;
    }

    /**
     * @return int
     */
    public function getColumnWidth(): int
    {
        return $this->columnWidth;
    }

    /**
     * @return int
     */
    public function getTextOrientation(): int
    {
        return $this->textOrientation;
    }
}