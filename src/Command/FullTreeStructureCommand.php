<?php

namespace App\Command;

use App\Model\CategoryTree;
use App\Repository\CategoryRepository;
use App\Service\Builder\CategoryTreeBuilder;
use App\Service\Formatter\CategoryTreeFormatter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FullTreeStructureCommand extends Command
{
    protected static $defaultName = 'app:listing1';

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var CategoryTreeFormatter
     */
    private $formatter;

    /**
     * @var CategoryTreeBuilder
     */
    private $builder;

    public function __construct(
        CategoryRepository $categoryRepository,
        CategoryTreeFormatter $formatter,
        CategoryTreeBuilder $builder
    ) {
        parent::__construct();

        $this->categoryRepository = $categoryRepository;
        $this->formatter = $formatter;
        $this->builder = $builder;
    }

    protected function configure()
    {
        $this->setDescription('Shows full category list.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $categories = $this->categoryRepository->findAll();

        $tree = $this->builder->build(new CategoryTree(), $categories);

        $output->write($this->formatter->format($tree));
    }
}