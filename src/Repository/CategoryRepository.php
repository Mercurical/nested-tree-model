<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @param int $left
     * @param int $right
     *
     * @return Category[]
     */
    public function getCategoryPathForBoundaries(int $left, int $right): array
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.lft <= :left')
            ->andWhere('c.rgt >= :right')
            ->orderBy('c.lft', 'ASC')
            ->setParameter('left', $left)
            ->setParameter('right', $right)
            ->getQuery();

        return $qb->execute();
    }

    /**
     * @return array
     */
    public function getProductsCountForCategories(): array
    {
        $qb = $this->createQueryBuilder('parent')
            ->select('parent.name, COUNT(product.product_id) AS count')
            ->leftJoin('App:Category', 'node', 'WITH', 'node.lft BETWEEN parent.lft AND parent.rgt')
            ->leftJoin('App:Product', 'product', 'WITH', 'node.id = product.category')
            ->groupBy('parent.name')
            ->orderBy('node.lft')
            ->getQuery();

        return $qb->execute();
    }
}
