<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Model\CategoryTree;
use App\Service\Builder\CategoryTreeBuilder;
use App\Service\Formatter\CategoryTreeFormatter;
use PHPUnit\Framework\TestCase;

class CategoryTreeFormatterTest extends TestCase
{
    /**
     * @return array
     */
    public function formatDataProvider(): array
    {
        $builder = new CategoryTreeBuilder();

        $tree1 = $builder->build(new CategoryTree(), []);
        $tree2 = $builder->build(new CategoryTree(), [
            (new Category())->setName('ELECTRONICS')->setLft(1)->setRgt(20),
            (new Category())->setName('COMPUTERS')->setLft(2)->setRgt(9),
            (new Category())->setName('NOTEBOOKS')->setLft(3)->setRgt(6),
            (new Category())->setName('ASUS')->setLft(4)->setRgt(5),
        ]);
        $tree3 = $builder->build(new CategoryTree(), [
            (new Category())->setName('ELECTRONICS')->setLft(1)->setRgt(20),
            (new Category())->setName('COMPUTERS')->setLft(2)->setRgt(9),
            (new Category())->setName('NOTEBOOKS')->setLft(3)->setRgt(6),
            (new Category())->setName('PHONES')->setLft(10)->setRgt(15),
            (new Category())->setName('CLOTHING')->setLft(21)->setRgt(50),
            (new Category())->setName('WOMEN')->setLft(22)->setRgt(35),
            (new Category())->setName('SWEATERS')->setLft(27)->setRgt(30),
        ]);

        return [
            [
                $tree1,
                ''
            ],
            [
                $tree2,
                "ELECTRONICS\n" .
                "  COMPUTERS\n" .
                "    NOTEBOOKS\n" .
                "      ASUS\n"
            ],
            [
                $tree3,
                "ELECTRONICS\n" .
                "  COMPUTERS\n" .
                "    NOTEBOOKS\n" .
                "  PHONES\n" .
                "CLOTHING\n" .
                "  WOMEN\n" .
                "    SWEATERS\n"
            ],
        ];
    }

    /**
     * @param CategoryTree $tree
     * @param string $expected
     * @throws \Exception
     *
     * @dataProvider formatDataProvider
     */
    public function testFormat(CategoryTree $tree, string $expected)
    {
        $formatter = new CategoryTreeFormatter();

        $this->assertEquals($expected, $formatter->format($tree));
    }
}