<?php

namespace App\Model;

class CategoryTreeNode
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $left;

    /**
     * @var int
     */
    private $right;

    /**
     * @var int
     */
    private $level;

    /**
     * @var CategoryTreeNode[]
     */
    private $childs = [];

    /**
     * @var CategoryTreeNode
     */
    private $parent;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return CategoryTreeNode
     */
    public function setName(string $name): CategoryTreeNode
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getLeft(): int
    {
        return $this->left;
    }

    /**
     * @param int $left
     *
     * @return CategoryTreeNode
     */
    public function setLeft(int $left): CategoryTreeNode
    {
        $this->left = $left;

        return $this;
    }

    /**
     * @return int
     */
    public function getRight(): int
    {
        return $this->right;
    }

    /**
     * @param int $right
     *
     * @return CategoryTreeNode
     */
    public function setRight(int $right): CategoryTreeNode
    {
        $this->right = $right;

        return $this;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     *
     * @return CategoryTreeNode
     */
    public function setLevel(int $level): CategoryTreeNode
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @param CategoryTreeNode $node
     *
     * @return CategoryTreeNode
     */
    public function addChild(CategoryTreeNode $node)
    {
        foreach ($this->childs as $child) {
            if ($child->getRight() > $node->getRight()) {
                $child->addChild($node->setLevel($node->getLevel() + 1));

                return $this;
            }
        }

        $node->setParent($this);
        $this->childs[$node->getRight()] = $node;
        ksort($this->childs);

        return $this;
    }

    /**
     * @return CategoryTreeNode[]
     */
    public function getChilds(): array
    {
        return $this->childs;
    }

    /**
     * @return CategoryTreeNode
     */
    public function getParent(): ?CategoryTreeNode
    {
        return $this->parent;
    }

    /**
     * @param CategoryTreeNode $parent
     *
     * @return CategoryTreeNode
     */
    public function setParent(CategoryTreeNode $parent): CategoryTreeNode
    {
        $this->parent = $parent;

        return $this;
    }
}