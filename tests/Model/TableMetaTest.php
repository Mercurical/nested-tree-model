<?php

namespace App\Tests\Model;

use App\Model\TableMeta;
use PHPUnit\Framework\TestCase;

class TableMetaTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $meta = new TableMeta('Name', 'category', 15, TableMeta::TEXT_LEFT);

        $this->assertEquals('Name', $meta->getColumnName());
        $this->assertEquals('category', $meta->getColumnKey());
        $this->assertEquals(15, $meta->getColumnWidth());
        $this->assertEquals(0, $meta->getTextOrientation());
    }
}