<?php

namespace App\Tests\Model;

use App\Model\CategoryTree;
use App\Model\CategoryTreeNode;
use PHPUnit\Framework\TestCase;

class CategoryTreeTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $categoryTree = new CategoryTree();

        $node = new CategoryTreeNode();
        $node
            ->setName('MP3')
            ->setLevel(0)
            ->setLeft(1)
            ->setRight(12);

        $categoryTree->addChild($node);
        $this->assertEquals($categoryTree->getChilds(), [12 => $node]);
    }
}