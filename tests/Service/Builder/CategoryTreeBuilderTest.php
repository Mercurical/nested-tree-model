<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Model\CategoryTree;
use App\Service\Builder\CategoryTreeBuilder;
use PHPUnit\Framework\TestCase;

class CategoryTreeBuilderTest extends TestCase
{
    /**
     * @return array
     */
    public function buildDataProvider(): array
    {
        return [
            [
                new CategoryTree(),
                [],
                [
                    null,
                    0
                ]
            ],
            [
                new CategoryTree(),
                [
                    (new Category())->setName('ELECTRONICS')->setLft(1)->setRgt(20),
                    (new Category())->setName('COMPUTERS')->setLft(2)->setRgt(9),
                    (new Category())->setName('NOTEBOOKS')->setLft(3)->setRgt(6),
                    (new Category())->setName('ASUS')->setLft(4)->setRgt(5),
                ],
                [
                    'ELECTRONICS',
                    1
                ]
            ],
            [
                new CategoryTree(),
                [
                    (new Category())->setName('ELECTRONICS')->setLft(1)->setRgt(20),
                    (new Category())->setName('COMPUTERS')->setLft(2)->setRgt(9),
                    (new Category())->setName('NOTEBOOKS')->setLft(3)->setRgt(6),
                    (new Category())->setName('PHONES')->setLft(10)->setRgt(15),
                    (new Category())->setName('CLOTHING')->setLft(21)->setRgt(50),
                    (new Category())->setName('WOMEN')->setLft(22)->setRgt(35),
                    (new Category())->setName('SWEATERS')->setLft(27)->setRgt(30),
                ],
                [
                    'ELECTRONICS',
                    2
                ]
            ],
        ];
    }

    /**
     * @param CategoryTree $tree
     * @param array $categories
     * @param array $expected
     * @throws \Exception
     *
     * @dataProvider buildDataProvider
     */
    public function testBuild(CategoryTree $tree, array $categories, array $expected)
    {
        $builder = new CategoryTreeBuilder();
        $result = $builder->build($tree, $categories);

        $childs = $result->getChilds();
        $firstChild = reset($childs) ?? null;
        $firstChildName = $firstChild ? $firstChild->getName() : null;
        $this->assertEquals($expected[0], $firstChildName);

        $firstChilds = $firstChild ? $firstChild->getChilds() : [];
        $this->assertEquals($expected[1], count($firstChilds));
    }
}