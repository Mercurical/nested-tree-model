<?php

namespace App\Tests\Model;

use App\Model\CategoryTreeNode;
use PHPUnit\Framework\TestCase;

class CategoryTreeNodeTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $node = new CategoryTreeNode();

        $node->setName('TROUSERS');
        $this->assertEquals('TROUSERS', $node->getName());

        $node->setLevel(3);
        $this->assertEquals(3, $node->getLevel());

        $node->setLeft(56);
        $this->assertEquals(56, $node->getLeft());

        $node->setRight(64);
        $this->assertEquals(64, $node->getRight());

        $parent = new CategoryTreeNode();
        $node->setParent($parent);
        $this->assertEquals($parent, $node->getParent());
    }
}