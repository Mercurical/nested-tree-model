<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $category = new Category();

        $category->setName('SHOES');
        $this->assertEquals('SHOES', $category->getName());

        $category->setLft(1);
        $this->assertEquals(1, $category->getLft());

        $category->setRgt(14);
        $this->assertEquals(14, $category->getRgt());
    }
}