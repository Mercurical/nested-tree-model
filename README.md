### Installation
1. Create file `.env.local` and set `DATABASE_URL` accordingly.
2. `composer install`

### Commands
1. `app:listing1` - full category tree
2. `app:listing2 [product_id]`, eg. `app:listing2 7` - breadcrumbs for a product
3. `app:listing3` - product count for every category

### Tests
`vendor/bin/phpunit`