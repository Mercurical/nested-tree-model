<?php

namespace App\Service\Builder;

use App\Entity\Category;
use App\Model\CategoryTree;
use App\Model\CategoryTreeNode;

class CategoryTreeBuilder
{
    /**
     * @param CategoryTree $tree
     * @param Category[] $categories
     *
     * @return CategoryTree
     */
    public function build(CategoryTree $tree, array $categories)
    {
        foreach ($categories as $category) {
            $tree->addChild(
                (new CategoryTreeNode())
                    ->setName($category->getName())
                    ->setLeft($category->getLft())
                    ->setRight($category->getRgt())
                    ->setLevel(0)
            );
        }

        return $tree;
    }
}