<?php

namespace App\Command;

use App\Model\TableMeta;
use App\Repository\CategoryRepository;
use App\Service\Formatter\TableFormatter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CategoryProducts extends Command
{
    protected static $defaultName = 'app:listing3';

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var TableFormatter
     */
    private $formatter;

    public function __construct(
        CategoryRepository $categoryRepository,
        TableFormatter $formatter
    ) {
        parent::__construct();

        $this->categoryRepository = $categoryRepository;
        $this->formatter = $formatter;
    }

    protected function configure()
    {
        $this->setDescription('Lists count of products for every category.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $categoryProducts = $this->categoryRepository->getProductsCountForCategories();

        $output->write(
            $this->formatter->format(
                $categoryProducts,
                [
                    new TableMeta('category', 'name', 22),
                    new TableMeta('product_count', 'count', 22, TableMeta::TEXT_LEFT)
                ]
            )
        );
    }
}