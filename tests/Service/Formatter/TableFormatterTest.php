<?php

namespace App\Tests\Entity;

use App\Model\TableMeta;
use App\Service\Formatter\TableFormatter;
use PHPUnit\Framework\TestCase;

class TableFormatterTest extends TestCase
{
    /**
     * @return array
     */
    public function tableDataProvider(): array
    {
        return [
            [
                [
                    ['aaa' => 1],
                    ['aaa' => 2],
                    ['aaa' => 6],
                    ['aaa' => 12],
                ],
                [new TableMeta('Count', 'aaa', 6, 0)],
                "+------+\n" .
                "| Count|\n" .
                "+------+\n" .
                "|    1 |\n" .
                "|    2 |\n" .
                "|    6 |\n" .
                "|   12 |\n" .
                "+------+\n"
            ],
            [
                [
                    ['aaa' => 'first', 'bbb' => 2, 'ccc' => 3],
                    ['aaa' => 'second', 'bbb' => 4, 'ccc' => 6],
                    ['aaa' => 'third', 'bbb' => 12, 'ccc' => 24],
                    ['aaa' => 'fourth', 'bbb' => 24, 'ccc' => 48],
                ],
                [
                    new TableMeta('Record number', 'aaa', 15, 0),
                    new TableMeta('Value 1', 'bbb', 10, 0),
                    new TableMeta('Value 2', 'ccc', 10, 0),
                ],
                "+---------------+----------+----------+\n" .
                "| Record number | Value 1  | Value 2  |\n" .
                "+---------------+----------+----------+\n" .
                "|         first |        2 |        3 |\n" .
                "|        second |        4 |        6 |\n" .
                "|         third |       12 |       24 |\n" .
                "|        fourth |       24 |       48 |\n" .
                "+---------------+----------+----------+\n"
            ]
        ];
    }

    /**
     * @param array $data
     * @param array $meta
     * @param string $expected
     * @throws \Exception
     *
     * @dataProvider tableDataProvider
     */
    public function testFormat(array $data, array $meta, string $expected)
    {
        $formatter = new TableFormatter();
        $result = $formatter->format($data, $meta);
        $this->assertEquals($expected, $result);
    }
}