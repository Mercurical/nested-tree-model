<?php

namespace App\Model;

class CategoryTree
{
    /**
     * @var CategoryTreeNode[]
     */
    private $childs = [];

    /**
     * @param CategoryTreeNode $node
     *
     * @return CategoryTree
     */
    public function addChild(CategoryTreeNode $node): CategoryTree
    {
        foreach ($this->childs as $child) {
            if ($child->getRight() > $node->getRight()) {
                $child->addChild($node->setLevel($node->getLevel() + 1));

                return $this;
            }
        }

        $this->childs[$node->getRight()] = $node;
        ksort($this->childs);

        return $this;
    }

    /**
     * @return CategoryTreeNode[]
     */
    public function getChilds(): array
    {
        return $this->childs;
    }
}