<?php

namespace App\Command;

use App\Model\CategoryTree;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Service\Builder\CategoryTreeBuilder;
use App\Service\Formatter\CategoryTreeFormatter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductCategoryPathCommand extends Command
{
    private const ARG_PRODUCT_ID = 'product_id';

    protected static $defaultName = 'app:listing2';

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var CategoryTreeFormatter
     */
    private $formatter;

    private $builder;

    public function __construct(
        CategoryRepository $categoryRepository,
        ProductRepository $productRepository,
        CategoryTreeFormatter $formatter,
        CategoryTreeBuilder $builder
    ) {
        parent::__construct();

        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->formatter = $formatter;
        $this->builder = $builder;
    }

    protected function configure()
    {
        $this
            ->setDescription('Shows category path for product with given ID.')
            ->addArgument(self::ARG_PRODUCT_ID, InputArgument::REQUIRED, 'ID of a product.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $productId = (int) $input->getArgument(self::ARG_PRODUCT_ID);
        if ($productId <= 0) {
            $this->shutdown($output, 'ID must be an integer.');
        }

        $product = $this->productRepository->find($productId);

        if (null === $product) {
            $this->shutdown($output, 'Product with given ID does not exists.');
        }

        $productCategory = $product->getCategory();
        $categories = $this->categoryRepository
            ->getCategoryPathForBoundaries($productCategory->getLft(), $productCategory->getRgt());

        $tree = $this->builder->build(new CategoryTree(), $categories);

        $output->write($this->formatter->format($tree));
    }

    /**
     * @param OutputInterface $output
     * @param string $message
     */
    private function shutdown(OutputInterface $output, string $message)
    {
        $output->write($message);

        exit(1);
    }
}